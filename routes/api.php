<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/**
 * Configure Route
 */
use App\Http\Controllers\Api\cao_usuarioController;
use App\Models\cao_usuario;

use App\Http\Controllers\Api\cao_faturaController;
use App\Models\cao_fatura;

use App\Http\Controllers\Api\cao_salarioController;
use App\Models\cao_salario;

use App\Http\Controllers\Api\cao_osController;
use App\Models\cao_os;

use App\Http\Controllers\Api\cao_sistemaController;
use App\Models\cao_sistema;

use App\Http\Controllers\Api\cao_clienteController;
use App\Models\cao_cliente;

use App\Http\Controllers\Api\permissao_sistemaController;
use App\Models\permissao_sistema;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// API MODEL cao_usuario
Route::apiResource('cao_usuario',cao_usuarioController::class);

// API MODEL cao_fatura
Route::apiResource('cao_fatura',cao_faturaController::class);

// API MODEL cao_salario
Route::apiResource('cao_salario',cao_salarioController::class);

// API MODEL cao_os
Route::apiResource('cao_os',cao_osController::class);

// API MODEL cao_sistema
Route::apiResource('cao_sistema',cao_sistemaController::class);

// API MODEL cao_cliente
Route::apiResource('cao_cliente',cao_clienteController::class);

// API MODEL permissao_sistema
Route::apiResource('permissao_sistema',permissao_sistemaController::class);
