@extends('adminlte::page')

@section('title', 'Painel')

@section('content_header')
    <h3>Controle de Atividades Online - Agence Interativa</h3>
@stop

@section('content')        
    <p>Bem-vindo a este belo painel de administração.</p>
@stop

@section('css')
    <link rel="stylesheet" href="/css/app.css">
@stop

@section('js')
    <script src="{{asset('js/app.js')}}"></script> <!--Añadimos el js generado con webpack, donde se encuentra nuestro componente vuejs-->
    <script> console.log('Hit'); </script>
    <script type="text/javascript" src="https://npmcdn.com/parse/dist/parse.min.js"></script>

@stop
