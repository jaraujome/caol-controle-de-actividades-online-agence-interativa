@extends('adminlte::page')

@section('title', 'DaskBoard')

@section('content_header')

@stop

@section('content')        
<div>
    <div id="app" data-app>
        <perforcomer></perforcomer>
    </div>

</div>     

@stop

@section('css')
    <link rel="stylesheet" href="/css/app.css">
@stop

@section('js')
    <script src="{{asset('js/app.js')}}"></script> <!--Añadimos el js generado con webpack, donde se encuentra nuestro componente vuejs-->
    <script> console.log('Hit'); </script>
    <script type="text/javascript" src="https://npmcdn.com/parse/dist/parse.min.js"></script> 
@stop
