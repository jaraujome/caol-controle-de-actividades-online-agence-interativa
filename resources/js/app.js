// app.js

/*require('./bootstrap');

window.Vue = require('vue');*/

/*Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});*/

/*import Vue from 'vue';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

const router = new VueRouter({ mode: 'history'});
new Vue(Vue.util.extend({ router })).$mount('#app');

import App from './App.vue';

new Vue(Vue.util.extend({ router }, App)).$mount('#app');*/



/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

import { createApp } from 'vue'
 
// libraries
import Datepicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css';
import { Bar, Doughnut } from 'vue-chartjs';
import Vue3VisualFilter from '@visual-filter/vue3';
var Vue3FiltersConfig = {
    capitalize: {
      onlyFirstLetter: false
    },
    number: {
      format: '0',
      thousandsSeparator: ',',
      decimalSeparator: '.'
    },
    bytes: {
      decimalDigits: 2
    },
    percent: {
      decimalDigits: 2,
      multiplier: 100,
      decimalSeparator: '.'
    },
    currency: {
      symbol: '$',
      decimalDigits: 2,
      thousandsSeparator: ',',
      decimalSeparator: '.',
      symbolOnLeft: true,
      spaceBetweenAmountAndSymbol: false,
      showPlusSign: false
    },
    pluralize: {
      includeNumber: false
    },
    ordinal: {
      includeNumber: false
    }
  }
//import 'vue3-date-time-picker/dist/main.css';

//Components

import perforcomer from './components/perforcomer/perforcomer.vue'

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const app = createApp({

el: '#app',

    Datepicker,
    Bar,
    Doughnut,
    Vue3VisualFilter,
    Vue3FiltersConfig,
    //Toaster,
    
    components: { 
        perforcomer,
     }
     
});
app.mount('#app');