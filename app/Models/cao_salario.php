<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cao_salario extends Model
{
    protected $table = 'cao_salario';

    //Relation One to One
    public function cao_usuario() {

        return $this->belongsTo(cao_usuario::class,'co_usuario','co_usuario');
    
    }

    /**
     * * Filter by Co_os 
     * @author José Araujo
     * @return Iluminate\Eloquent\Builder
     */
    public function scopeFilterQueryco_os($query, $co_os){    
        // Convierte a un Array
        $arrco_os = explode(",", $co_os);
        return $query->whereIn('co_usuario', $arrco_os);
        
   }    
}
