<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cao_os extends Model
{
    protected $table = 'cao_os';

    //Relation One to Many
    public function cao_fatura() {
    
        return $this->hasMany(cao_fatura::class,'co_os','co_os');

    }

    //Relation One to Many (Inverse)
    public function cao_usuario() {
    
        return $this->belongsTo(cao_usuario::class,'co_os','co_os');
 
     }        

}
