<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cao_usuario extends Model
{
    protected $table = 'cao_usuario';

    //Relation One to One
    public function permissao_sistema() {
    
        return $this->hasOne(permissao_sistema::class,'co_usuario','co_usuario');

    }

    //Relation One to Many
    public function cao_os() {
    
        return $this->hasMany(cao_os::class,'co_usuario','co_usuario');

    }    

   //Relation One a One
   public function cao_salario() {
    
    return $this->hasOne(cao_salario::class,'co_usuario','co_usuario');

}    

    /**
     * * Filter by Permissao
     * @author José Araujo
     * @return Iluminate\Eloquent\Builder
     */
    public function scopeFilterPermissao($query){       
        return $query->whereHas("permissao_sistema", function($query) {
            $query->where("co_sistema", "=", 1)->where("in_ativo","=","S")->whereIn("co_tipo_usuario", [0, 1, 2]);
                    
        });
   }  

}
