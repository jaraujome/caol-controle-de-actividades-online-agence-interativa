<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class permissao_sistema extends Model
{
    protected $table = 'permissao_sistema';

    //Relation One to One
    public function cao_usuario() {

    return $this->belongsTo(cao_usuario::class,'co_usuario','co_usuario');
    
    }
}
