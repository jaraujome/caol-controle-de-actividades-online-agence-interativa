<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cao_cliente extends Model
{
    protected $table = 'cao_cliente';

    //Relation One to Many
     public function cao_fatura() {
    
        return $this->hasMany(cao_fatura::class,'co_cliente','co_cliente');

    }

}
