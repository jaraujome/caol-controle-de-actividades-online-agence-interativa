<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cao_fatura extends Model
{
    protected $table = 'cao_fatura';

     //Relation One to Many (Inverse)
     public function cao_cliente() {
    
        return $this->belongsTo(cao_cliente::class,'co_cliente','co_cliente');

    }

     //Relation One to Many (Inverse)
     public function cao_os() {
    
        return $this->belongsTo(cao_os::class,'co_os','co_os');

    }    

    //Relation One to Many (Inverse)
    public function cao_sistema() {
    
       return $this->belongsTo(cao_sistema::class,'co_sistema','co_sistema');

    }    

    /**
     * * Filter by Data_Emissao
     * @author José Araujo
     * @return Iluminate\Eloquent\Builder
     */
    public function scopeFilterDataemissao($query, $dataemi){    
        //die($dataemi);->where("co_usuario", "$co_usuario")
        if (!empty($dataemi) ) {
             return $query->where('data_emissao',"$dataemi");
        
        }

   }

}
