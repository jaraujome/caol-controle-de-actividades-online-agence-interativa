<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\cao_clienteStoreRequest;
use App\Http\Resources\cao_clienteResource;
use App\Models\cao_cliente;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class cao_clienteController extends Controller
{
    //API-CONTROLLER cao_cliente
    public function index()
    {   
        return cao_clienteResource::collection(cao_cliente::all());
    }
}
