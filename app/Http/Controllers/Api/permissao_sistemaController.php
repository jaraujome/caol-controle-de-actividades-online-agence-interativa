<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\permissao_sistemaStoreRequest;
use App\Http\Resources\permissao_sistemaResource;
use App\Models\permissao_sistema;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class permissao_sistemaController extends Controller
{
    //API-CONTROLLER permissao_sistema
    public function index()
    {   
        return permissao_sistemaResource::collection(permissao_sistema::all());
    }
}
