<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\cao_sistemaStoreRequest;
use App\Http\Resources\cao_sistemaResource;
use App\Models\cao_sistema;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class cao_sistemaController extends Controller
{
    //API-CONTROLLER cao_sistema
    public function index()
    {   
        return cao_sistemaResource::collection(cao_sistema::all());
    }
}
