<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\cao_faturaStoreRequest;
use App\Http\Resources\cao_faturaResource;
use App\Models\cao_fatura;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
//use App\Http\Api\DB;
use Illuminate\Http\Request;

class cao_faturaController extends Controller
{
    //API-CONTROLLER cao_fatura
    public function index(Request $request)
    {   

        /**
         * Controller Principal de Factura
         * @author José Araujo
         */
        $findemi = $request->input('data_emissao');
        $findusuario = $request->input('consultors');
        $arrco_usuario = explode(",", $findusuario);
        $queryfatura = DB::table('cao_fatura')
        ->join('cao_cliente', 'cao_fatura.co_cliente', '=', 'cao_cliente.co_cliente')
        ->join('cao_sistema', 'cao_fatura.co_sistema', '=', 'cao_sistema.co_sistema')
        ->join('cao_os', 'cao_fatura.co_os', '=', 'cao_os.co_os')
        ->join('cao_usuario', 'cao_os.co_usuario', '=', 'cao_usuario.co_usuario')
        ->join('permissao_sistema', 'cao_usuario.co_usuario', '=', 'permissao_sistema.co_usuario')
        ->join('cao_salario', 'cao_usuario.co_usuario', '=', 'cao_salario.co_usuario')
        ->where('permissao_sistema.co_sistema', '=', 1)
        ->where('permissao_sistema.in_ativo', '=', 'S')
        ->whereIn('permissao_sistema.co_tipo_usuario', [1,2.3])
        ->whereIn('cao_usuario.co_usuario', $arrco_usuario)
        ->orwhere('cao_fatura.data_emissao', '=', $findemi);

        return cao_faturaResource::collection($queryfatura->get()->all());
        
    }
}
