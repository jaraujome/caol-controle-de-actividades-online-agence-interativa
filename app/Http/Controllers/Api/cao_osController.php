<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\cao_osStoreRequest;
use App\Http\Resources\cao_osResource;
use App\Models\cao_os;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class cao_osController extends Controller
{
    //API-CONTROLLER cao_os
    public function index()
    {   
        return cao_osResource::collection(cao_os::all());
    }
}
