<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\cao_salarioStoreRequest;
use App\Http\Resources\cao_salarioResource;
use App\Models\cao_salario;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class cao_salarioController extends Controller
{
    //API-CONTROLLER cao_salario
    public function index()
    {   
        return cao_salarioResource::collection(cao_salario::all());
    }
    

}
