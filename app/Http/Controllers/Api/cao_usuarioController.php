<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\cao_usuarioStoreRequest;
use App\Http\Resources\cao_usuarioResource;
use App\Models\cao_usuario;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class cao_usuarioController extends Controller
{
    //API-CONTROLLER cao_usuario
    public function index(Request $request)
    {   
        return cao_usuarioResource::collection(cao_usuario::FilterPermissao()->get()->all());
    }
}
