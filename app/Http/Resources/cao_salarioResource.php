<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class cao_salarioResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            'co_usuario' => $this->co_usuario,
            'dt_alteracao' => $this->dt_alteracao,
            'brut_salario' => $this->brut_salario,
            'liq_salario' => $this->liq_salario, 
                       
        ];
    }
}
