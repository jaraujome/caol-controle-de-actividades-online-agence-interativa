<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class permissao_sistemaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            'co_usuario' => $this->co_usuario,
            'co_tipo_usuario => $this->co_tipo_usuario',
            'co_sistema' => $this->co_sistema,
            'in_ativo' => $this->in_ativo,
            'co_usuario_atualizacao' => $this->co_usuario_atualizacao,
            'dt_atualizacao' => $this->dt_atualizacao,
        ];
    }
}