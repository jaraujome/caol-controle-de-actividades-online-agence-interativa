<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class cao_osResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            'co_os' => $this->co_os, 
            'nu_os'=> $this->nu_os,
            'co_sistema'=> $this->co_sistema,
            'co_usuario'=> $this->co_usuario,
            'co_arquitetura'=> $this->co_arquitetura,
            'ds_os'=> $this->ds_os,
            'ds_caracteristica'=> $this->ds_caracteristica,
            'ds_requisito'=> $this->ds_requisito,
            'dt_inicio'=> $this->dt_inicio,
            'dt_fim'=> $this->dt_fim,
            'co_status'=> $this->co_status,
            'diretoria_sol'=> $this->diretoria_sol,
            'dt_sol'=> $this->dt_sol,
            'nu_tel_sol'=> $this->nu_tel_sol,
            'ddd_tel_sol'=> $this->ddd_tel_sol,
            'nu_tel_sol2'=> $this->nu_tel_sol2,
            'ddd_tel_sol2'=> $this->ddd_tel_sol2,
            'usuario_sol'=> $this->usuario_sol,
            'dt_imp'=> $this->dt_imp,
            'dt_garantia'=> $this->dt_garantia,
            'co_email'=> $this->co_email,
            'co_os_prospect_rel'=> $this->co_os_prospect_rel,                       
        ];

    }
}
