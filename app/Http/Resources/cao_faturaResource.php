<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class cao_faturaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            'co_usuario' => $this->co_usuario,
            'no_usuario' => $this->no_usuario,
            'co_fatura' => $this->co_fatura,
            'co_cliente' => $this->co_cliente,
            'co_tipo_usuario' => $this->co_tipo_usuario,
            'co_sistema' => $this->co_sistema,
            'in_ativo' => $this->in_ativo,
            'co_os' => $this->co_os,
            'num_nf' => $this->num_nf,
            'total' => $this->total,
            'valor' => $this->valor,
            'data_emissao' => $this->data_emissao,
            'corpo_nf' => $this->corpo_nf,
            'comissao_cn' => $this->comissao_cn,
            'total_imp_inc' => $this->total_imp_inc,
            'brut_salario' => $this->brut_salario,
            'receta' => ($this->valor * $this->total_imp_inc),
            'comision' => (($this->valor - ($this->valor * $this->total_imp_inc) * $this->comissao_cn)),
            'lucro' => ($this->valor * $this->total_imp_inc) - ($this->brut_salario + $this->comissao_cn),
        ];
    }
}
