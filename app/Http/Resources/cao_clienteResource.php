<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class cao_clienteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            'co_cliente' => $this->co_cliente,
            'no_razao' => $this->no_razao,
            'no_fantasia' => $this->no_fantasia,
            'no_contato' => $this->no_contato,
            'nu_telefone' => $this->nu_telefone,
            'nu_ramal' => $this->nu_ramal,
            'nu_cnpj' => $this->nu_cnpj,
            'ds_endereco' => $this->ds_endereco,
            'nu_numero' => $this->nu_numero,
            'ds_complemento' => $this->ds_complemento,
            'no_bairro' => $this->no_bairro,
            'nu_cep' => $this->nu_cep,
            'no_pais' => $this->no_pais,
            'co_ramo' => $this->co_ramo,
            'co_cidade' => $this->co_cidade,
            'co_status' => $this->co_status,
            'ds_site' => $this->ds_site,
            'ds_email' => $this->ds_email,
            'ds_cargo_contato' => $this->ds_cargo_contato,
            'tp_cliente' => $this->tp_cliente,
            'ds_referencia' => $this->ds_referencia,
            'co_complemento_status' => $this->co_complemento_status,
            'nu_fax' => $this->nu_fax,
            'ddd2' => $this->ddd2,
            'telefone2' => $this->telefone2,
        ];
    }
}


