<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class cao_sistemaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'co_sistema' => $this->co_sistema,
            'co_cliente' => $this->co_cliente,
            'co_usuario' => $this->co_usuario,
            'co_arquitetura' => $this->co_arquitetura,
            'no_sistema' => $this->no_sistema,
            'ds_sistema_resumo' => $this->ds_sistema_resumo,
            'ds_caracteristica' => $this->ds_caracteristica,
            'ds_requisito' => $this->ds_requisito,
            'no_diretoria_solic' => $this->no_diretoria_solic,
            'ddd_telefone_solic' => $this->ddd_telefone_solic,
            'nu_telefone_solic' => $this->nu_telefone_solic,
            'no_usuario_solic' => $this->no_usuario_solic,
            'dt_solicitacao' => $this->dt_solicitacao,
            'dt_entrega' => $this->dt_entrega,
            'co_email' => $this->co_email,
        ];
    }
}
